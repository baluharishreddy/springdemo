package com.hcl.spring;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class AppClass 
{
	public static void main(String[] args)
	{
		//Resource resource=new ClassPathResource("Spring-application.xml");
		//BeanFactory factory=new XmlBeanFactory(resource);
		
		ApplicationContext context=new ClassPathXmlApplicationContext("Spring-application.xml");
		Employee employee=(Employee) context.getBean("employeeBean");
		System.out.println(employee.getId());
		System.out.println(employee.getName());
		System.out.println(employee.getDepartment().getDepId());
		System.out.println(employee.getDepartment().getDepName());
	}

}
